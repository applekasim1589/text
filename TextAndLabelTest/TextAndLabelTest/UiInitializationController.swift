//
//  UiInitializationController.swift
//  ElectronicReceipt
//
//  Created by 周政翰 on 2017/8/24.
//  Copyright © 2017年 kasim. All rights reserved.
//

import UIKit


class UiInitializationController: NSObject {

    weak var currentView:UIView!
    var composes = [UITextField: UILabel]()
    var labelVerticalConstraint = [UILabel: NSLayoutConstraint]()
    var labelHorizontalConstraint = [UILabel: NSLayoutConstraint]()
    
    init(currentView:UIView) {
        self.currentView = currentView
    }
    
    
    func compose(label:UILabel, targetTextField:UITextField) {
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let horConstraint = NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: targetTextField, attribute: .leading, multiplier: 1.0, constant: 0.0)
        
        let verConstraint = NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: targetTextField, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        
        NSLayoutConstraint.activate([horConstraint, verConstraint])
        composes[targetTextField] = label
        labelVerticalConstraint[label] = verConstraint
        labelHorizontalConstraint[label] = horConstraint
        
    }
    
    func editingDidEnd(_ sender: UITextField) {
        
            let label:UILabel = self.composes[sender]!
            let verticalConstraint:NSLayoutConstraint = self.labelVerticalConstraint[label]!
            let horizontalConstraint:NSLayoutConstraint = self.labelHorizontalConstraint[label]!
         
            UIView.animate(withDuration: 1, animations: {[unowned self] in
                
                verticalConstraint.constant += 30
                horizontalConstraint.constant += label.frame.size.width / 2
                
                label.textColor = .darkGray
                
                label.transform = label.transform.scaledBy(x: 2, y: 2)
                self.currentView.layoutIfNeeded()
                }, completion: nil)
        
    }
    
    func touchDown(_ sender: UITextField) {
        
            let label:UILabel = self.composes[sender]!
            let verticalConstraint:NSLayoutConstraint = self.labelVerticalConstraint[label]!
            let horizontalConstraint:NSLayoutConstraint = self.labelHorizontalConstraint[label]!
           
            UIView.animate(withDuration: 1, animations: {[unowned self] in
                
                verticalConstraint.constant -= 30
                horizontalConstraint.constant -= label.frame.size.width / 4
                
                label.transform = label.transform.scaledBy(x: 0.5, y: 0.5)
                
                label.textColor = .blue
                
                self.currentView.layoutIfNeeded()
                }, completion: nil)
        
    }
}

extension UITextField {
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

