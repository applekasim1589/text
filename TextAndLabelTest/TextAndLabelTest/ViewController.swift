//
//  ViewController.swift
//  TextAndLabelTest
//
//  Created by 周政翰 on 2017/8/25.
//  Copyright © 2017年 kasim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var password: UILabel!
    @IBOutlet weak var pLineText: UITextField!
    @IBOutlet weak var account: UILabel!
    @IBOutlet weak var lineText: UITextField!


    
    var uiInitializationController:UiInitializationController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiInitializationController = UiInitializationController(currentView: self.view)
        lineText.underlined()
        lineText.tintColor = UIColor.lightGray
        
        pLineText.underlined()
        pLineText.tintColor = UIColor.lightGray
        
        uiInitializationController.compose(label:account, targetTextField:lineText)
        uiInitializationController.compose(label:password, targetTextField:pLineText)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func touDoen(_ sender: UITextField) {
        
        if sender.text == "" {
            uiInitializationController.touchDown(sender)
        }
    }
    
    @IBAction func touDoen2(_ sender: UITextField) {
        
        if sender.text == "" {
            uiInitializationController.touchDown(sender)
        }
    }
    
    @IBAction func edend(_ sender: UITextField) {
        if sender.text == "" {
            uiInitializationController.editingDidEnd(sender)
        }
    }
    
    @IBAction func edend2(_ sender: UITextField) {
        if sender.text == "" {
            uiInitializationController.editingDidEnd(sender)
        }
    }
    
}

